import {
  Controller,
  HostParam,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path = require('path');
import { config } from 'process';
import { IMAGE_FOLDER, STATIC_SERVE_FOLDER } from 'src/shared/constants';
import { FileHandlerService } from './file-handler.service';
import { existsSync, mkdirSync } from 'fs';
@Controller('file-handler')
export class FileHandlerController {
  constructor(
    private fileHandlerService: FileHandlerService,
    private config: ConfigService,
  ) {}

  @HttpCode(HttpStatus.OK)
  @Post('upload-image/:path')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        // destination: `${STATIC_SERVE_FOLDER}${IMAGE_FOLDER}`,
        destination: async (req, file, callback) => {
          let dir = `${STATIC_SERVE_FOLDER}${IMAGE_FOLDER}${req.params.path}`;
          const res = mkdirSync(dir, { recursive: true });
          console.log(res);

          callback(null, dir);
        },
        filename: (req, file, cb) => {
          const filename: string =
            path.parse(file.originalname).name.replace(/\s/g, '') +
            '-' +
            Date.now();
          const extension: string = path.parse(file.originalname).ext;

          cb(null, `${filename}${extension}`);
        },
      }),
    }),
  )
  uploadFile(@UploadedFile() file: Express.Multer.File, @Param('path') path) {
    return {
      ...file,
      link: `${this.config.get('host')}/${IMAGE_FOLDER}${path}/${
        file.filename
      }`,
    };
    //   }
  }
}
