import { Module } from '@nestjs/common';
import { FileHandlerService } from './file-handler.service';
import { FileHandlerController } from './file-handler.controller';

@Module({
  providers: [FileHandlerService],
  controllers: [FileHandlerController]
})
export class FileHandlerModule {}
