import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileHandlerModule } from './file-handler/file-handler.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import configuration from 'config/configuration';
import { ROOT_PATH } from './shared/constants';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `./config/env/${process.env.NODE_ENV}.env`,
      load: [configuration],
    }),
    ServeStaticModule.forRoot({
      rootPath: `${ROOT_PATH}/public`,
    }),
    FileHandlerModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigService],
})
export class AppModule {}
