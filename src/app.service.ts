import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(private config: ConfigService) {}
  getHello() {
    return {
      mess: 'Hello world',
      data: this.config.get('app_port'),
    };
  }
}
