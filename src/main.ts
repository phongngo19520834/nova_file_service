import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ROOT_PATH } from './shared/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  const port = process.env.PORT || 1;
  await app.listen(port, () => {
    const logger = new Logger('BOOSTRAP');
    logger.log(`App is listening in port: ${port}`);
    logger.log(`NODE_ENV: ${process.env.NODE_ENV}`);
    logger.log(`Working directory: ${process.cwd()}`);
    logger.log(`Root path: ${ROOT_PATH}`);
  });
}
bootstrap();
