export default () => ({
  app_port: { port: parseInt(process.env.PORT, 10) || 2999 },
  host: process.env.HOST,
});
